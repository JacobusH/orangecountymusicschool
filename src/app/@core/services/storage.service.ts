import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { Advert, Upload } from 'app/@core/models/_index';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { finalize } from 'rxjs/operators';
import 'rxjs/add/operator/switchMap';

@Injectable({ providedIn: 'root' })
export class StorageService {
  adverts: AngularFirestoreCollection<Advert>;
  advertsActive: AngularFirestoreCollection<Advert>;
  // storage = firebase.storage();
  storageRef = this.storage.ref('adverts/');
  forceAdvert: AngularFirestoreDocument<{'forceShow': boolean, 'isAdvert': boolean}>;

  constructor(private afs: AngularFirestore
    , private storage: AngularFireStorage ) {
    this.adverts = this.afs.collection('adverts', ref => ref.where('isAdvert', '==', true));
    this.advertsActive = this.afs.collection('adverts', ref => ref.where('isActive', '==', true));
    this.forceAdvert = this.afs.collection('adverts').doc('!advertSwitch');
  }

  uploadFile(file, bucketName): AngularFireUploadTask {
    // file = event.target.files[0];
    // const filePath = 'name-your-file-path-here';

    let fileRef = this.storage.ref(bucketName);
    let task = this.storage.upload(bucketName, file);
    task.snapshotChanges().pipe(
      finalize(() => {
        fileRef.getDownloadURL()
      })
    )
    .subscribe()

    return task;
  }
    // this.uploadPercent = task.percentageChanges();





}

// <input type="file" (change)="uploadFile($event)" />
// <div>{{ uploadPercent | async }}</div>
// <a [href]="downloadURL | async">{{ downloadURL | async }}</a>



