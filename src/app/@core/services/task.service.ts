import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { Task } from 'app/@core/models/_index';
import 'rxjs/add/operator/switchMap'
import * as firebase from 'firebase/app';

@Injectable({ providedIn: 'root' })
export class TaskService {
  tasks: AngularFirestoreCollection<Task>;

  constructor(private afs: AngularFirestore) {
    this.tasks = this.afs.collection('tasks');
  }

  createNew(): Task {
    let data: Task = {
      key: '',
      name: '',
      descriptionHtml: '',
      notes: new Array,
      isActive: true,
      createdAt: new Date(),
      updatedAt: new Date()
      };
      return data;
  }


  save(t: Task): Promise<firebase.firestore.DocumentReference>  {
    let promise: Promise<firebase.firestore.DocumentReference> = this.tasks.add(t);
    promise.then(x => {
      x.update({key: x.id});
    });

    return promise;
  }

  edit(item: Task): Promise<void> {
    return this.tasks.doc(item.key).update(item);
  }

  delete(item: Task): Promise<void> {
    return this.tasks.doc(item.key).delete();
  }

}
