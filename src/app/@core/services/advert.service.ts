import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { Advert, Upload } from 'app/@core/models/_index';
import { AngularFireStorage } from '@angular/fire/storage';
import 'rxjs/add/operator/switchMap';

@Injectable({ providedIn: 'root' })export class AdvertService {
  adverts: AngularFirestoreCollection<Advert>;
  advertsActive: AngularFirestoreCollection<Advert>;
  // storage = firebase.storage();
  // storageRef = this.storage.ref('adverts/');
  forceAdvert: AngularFirestoreDocument<{'forceShow': boolean, 'isAdvert': boolean}>;

  constructor(private afs: AngularFirestore
    , private storage: AngularFireStorage) {
      this.adverts = this.afs.collection('adverts', ref => ref.where('isAdvert', '==', true));
      this.advertsActive = this.afs.collection('adverts', ref => ref.where('isActive', '==', true));
      this.forceAdvert = this.afs.collection('adverts').doc('!advertSwitch');
  }

  createNew(): Advert {
    let data: Advert = {
      key: '',
      name: '',
      imgUrl: '',
      isActive: true,
      createdAt: new Date(),
      updatedAt: new Date(),
      isAdvert: true
      };
      return data;
  }

  save(t: Advert, up: Upload)   {
    // let promise: Promise<firebase.firestore.DocumentReference> = this.adverts.add(t);
    // promise.then(x => {
    //   x.update({key: x.id});

    //   let itemRef = this.storageRef.child('adverts/' + up.name);
    //   itemRef.getDownloadURL().then((url) => {
    //     // this.selectedPicture = url;
    //     this.advertsActive.doc(x.id).update({imgUrl: url});
    //   })
    //   .catch((err) => {
    //     console.log(err);
    //   });
    // });
  }

  edit(item: Advert): Promise<void> {
    return this.adverts.doc(item.key).update(item);
  }

  delete(item: Advert): Promise<void> {
    return this.adverts.doc(item.key).delete();
  }

  flipSwitch(currentState: boolean) {
    let newState = !currentState;
    this.afs.doc('adverts/!advertSwitch').update({'forceShow': newState});
  }

}
