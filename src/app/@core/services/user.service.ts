import { Injectable, OnDestroy } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs/Observable';
import { User, Roles } from 'app/@core/models/user.model';
import { map, first, flatMap } from 'rxjs/operators';
import 'rxjs/add/operator/switchMap'
import * as firebase from 'firebase/app';
import { Subscription } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService implements OnDestroy {
  users: AngularFirestoreCollection<User>;
  u$: Subscription;

  constructor(private afs: AngularFirestore) {
    this.users = this.afs.collection('users');
  }

  ngOnDestroy() {
    this.u$.unsubscribe();
  }

  createNew(): User {
    let roles: Roles = {
      superAdmin: false,
      admin: false,
      teacher: false,
      student: false,
      guest: true
    };
    let data: User = {
      authID: '',
      authMethod: '',
      authDisplayName: '' ,
      authPhotoUrl: '',
      key: '',
      name: '',
      email: '',
      phone: '',
      roles: roles,
      isActive: true,
      createdAt: new Date(),
      updatedAt: new Date()
      };
      return data;
  }

  saveNewUser(us: User): Promise<void> {
    return this.users.doc(us.authID).set(us);
  }

  getByKey(key: string) {
    return this.afs.collection(`users/${key}`).valueChanges();
  }

  getByAuthId(authId: string) {
    return this.afs.collection('users', ref => ref.where('authID', '==', authId).limit(1)).valueChanges().pipe(flatMap(x => x));
  }

  getByEmail(email: string) {
    return this.afs.collection('users', ref => ref.where('email', '==', email)).valueChanges();
  }

  edit(item: User): Promise<void> {
    return this.users.doc(item.key).update(item);
  }

  updateURL(item: User, url: string): Promise<void> {
    //  this.afs.doc('teachers/' + item.key).update({imgUrl: url});
    return this.users.ref.doc(item.key).update({imgUrl: url});
  }

  delete(item: User): Promise<void> {
    return this.users.doc(item.key).delete();
  }

  updateUserData(userAuthCreds, provider) {
    // only gets set on create of a user
    const data: User = {
      authID: userAuthCreds.uid,
      authMethod: provider.providerId,
      authDisplayName: userAuthCreds.displayName,
      authPhotoUrl: userAuthCreds.photoURL,
      key: userAuthCreds.uid,
      name: userAuthCreds.displayName,
      phone: userAuthCreds.phone,
      email: userAuthCreds.email,
      roles: {'superAdmin': false, 'admin': false, 'teacher': false, 'student': false, 'guest': true}, // everyone starts as a guest
      isActive: true,
      createdAt: new Date(),
      updatedAt: new Date()
    }

    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${userAuthCreds.uid}`);
    this.u$ = userRef.snapshotChanges().pipe(map(action => action.payload.exists))
      .subscribe(exists => exists
        ? console.log('user exists')//userRef.update(data)
        : userRef.set(data))
  }

}
