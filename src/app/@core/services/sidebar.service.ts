import { Injectable } from '@angular/core';
import { Observable, Subject, BehaviorSubject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class SidebarService {
  isSidebarToggleShow = false;

  constructor() {

  }

  setStatus(b: boolean) {
    this.isSidebarToggleShow = b;
  }

  toggle() {
    this.isSidebarToggleShow = !this.isSidebarToggleShow;
  }

}
