import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import * as firebase from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { UserService } from 'app/@core/services/user.service';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs';
import { User } from '../models/user.model';
import { tap, map } from 'rxjs/operators';
import { AlertService } from './alert.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  firebaseUser: any;
  user$: Observable<User>;
  errors$: Observable<string[]> = new Observable<string[]>();

  constructor(public afAuth: AngularFireAuth
              , private afs: AngularFirestore
              , private router: Router
              , private userService: UserService
              , private alertService: AlertService) {
      //// Get auth data, then get firestore user document || null
      this.user$ = this.afAuth.authState
      .switchMap(user => {
        if (user) {
          return this.userService.getByAuthId(user.uid)
          // return this.afs.doc<User>(`users/${user.uid}`).valueChanges()
        } else {
          return of(null)
        }
      })
  }


  /***
  *** Login/Signup
  ***/
  emailSignup(user: User, pass: string) {
    const provider = new firebase.auth.EmailAuthProvider();
    this.afAuth.createUserWithEmailAndPassword(user.email, pass).then(userCred => {
      user.authMethod = 'email';
      user.authDisplayName = user.name;
      user.authID = userCred.user.uid;
      user.key = user.authID;
      this.userService.saveNewUser(user);
    }).then(x => {
      this.router.navigate(['/user/profile']);
    }).catch(err => {
      console.log("EMAIL REGISTER ERROR");
      console.log(err);
    })


    // this.afAuth.auth.createUserWithEmailAndPassword(user.email, pass).then(userCred => {
    //   // worked, afAuth.authState will change now
    //   user.authMethod = "email";
    //   user.authDisplayName = user.fullName;
    //   this.userService.saveNewUser(user).then(x => {
    //     let xx: any = x;
    //     user.authID = xx.firestore._credentials.currentUser.uid;
    //     user.key = xx._key.path.segments[1];
    //     this.userService.edit(user);
    //     this.router.navigate(['/user/profile']);
    //   });
    // })
    // .catch(err => {
    //   console.log("EMAIL REGISTER ERROR");
    //   console.log(err);
    // })
  }

  emailLogin(username, pass) {
    this.afAuth.signInWithEmailAndPassword(username, pass).then(x => {
      // worked, afAuth.authState will change now
      this.firebaseUser = x.user;
      this.router.navigate(['/user/profile']);
      console.log("Login successful");
    })
    .catch(err => {
      console.log("EMAIL LOGIN ERROR");
      console.log(err);
      this.alertService.clear();
      this.alertService.error(err.message);
    })
  }

  googleLogin() {
    const provider = new firebase.auth.GoogleAuthProvider()
    return this.oAuthLogin(provider);
  }

  private oAuthLogin(provider) {
    return this.afAuth.signInWithPopup(provider)
    .then((credential) => {
      this.userService.updateUserData(credential.user, provider);
    })
    .catch(err => {
      this.alertService.clear();
      this.alertService.error(err.message);
      console.log('login error: ' + err);
    })
  }

  signOut() {
    this.afAuth.signOut().then(() => {
      this.router.navigate(['/']);
    });
  }

  /***
  *** Role-based Authorization
  ***/
  canRead(user: User): boolean {
    const allowed = ['admin', 'editor', 'subscriber']
    return this.checkAuthorization(user, allowed)
  }

  canEdit(user: User): boolean {
    const allowed = ['admin', 'editor']
    return this.checkAuthorization(user, allowed)
  }

  canDelete(user: User): boolean {
    const allowed = ['admin']
    return this.checkAuthorization(user, allowed)
  }

  // determines if user has matching role
  private checkAuthorization(user: User, allowedRoles: string[]): boolean {
    if (!user) return false
    for (const role of allowedRoles) {
      if ( user.roles[role] ) {
        return true
      }
    }
    return false
  }

}
