import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { FAQ } from 'app/@core/models/_index';
import 'rxjs/add/operator/switchMap'

@Injectable({ providedIn: 'root' })
export class FAQService {
  faqs: AngularFirestoreCollection<FAQ>;
  faqsActive: AngularFirestoreCollection<FAQ>;

  constructor(private afs: AngularFirestore) {
    this.faqs = this.afs.collection('faqs');
    this.faqsActive = this.afs.collection('faqs', ref => ref.where('isActive', '==', 'true'));
  }

  createNew(): FAQ {
    let data: FAQ = {
      key: '',
      question: '',
      answer: '',
      isActive: true,
      createdAt: new Date(),
      updatedAt: new Date()
      };
      return data;
  }

  save(t: FAQ): Promise<firebase.firestore.DocumentReference>  {
    let promise: Promise<firebase.firestore.DocumentReference> = this.faqs.add(t);
    promise.then(x => {
      x.update({key: x.id});
    });

    return promise;
  }

  edit(item: FAQ): Promise<void> {
    return this.faqs.doc(item.key).update(item);
  }

  delete(item: FAQ): Promise<void> {
    return this.faqs.doc(item.key).delete();
  }

}
