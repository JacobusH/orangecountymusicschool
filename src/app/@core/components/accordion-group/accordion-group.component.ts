import { Component, OnInit, Input } from '@angular/core';
import { FAQ } from 'app/@core/models/_index';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-accordion-group',
  templateUrl: './accordion-group.component.html',
  styleUrls: ['./accordion-group.component.scss']
})
export class AccordionGroupComponent implements OnInit {
  @Input() data: Observable<FAQ>;

  constructor() { }

  ngOnInit(): void {
  }

}
