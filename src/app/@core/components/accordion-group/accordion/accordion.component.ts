import { Component, OnInit, Input } from '@angular/core';
import { slideInOutAnimation, listAnimation } from 'app/@core/animations/_index';

@Component({
  selector: 'app-accordion',
  templateUrl: './accordion.component.html',
  styleUrls: ['./accordion.component.scss'],
  animations: [slideInOutAnimation, listAnimation]
})
export class AccordionComponent implements OnInit {
  @Input() header: string;
  @Input() body: string;
  isDown = false;

  constructor() { }

  ngOnInit(): void {

  }

}
