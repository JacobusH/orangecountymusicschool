import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Component, OnInit, ViewEncapsulation, Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { Teacher } from 'app/@core/models/_index';
import { TeacherService } from 'app/@core/services/_index';
import * as firebase from 'firebase/app';
import 'rxjs/add/operator/switchMap'

@Component({
  selector: 'app-display-teacher',
  templateUrl: './display-teacher.component.html',
  styleUrls: ['./display-teacher.component.scss'],
  encapsulation: ViewEncapsulation.Emulated
})
export class DisplayTeacherComponent implements OnInit {
  teachers: Observable<Teacher[]>;

  constructor(private afs: AngularFirestore
    , private teacherService: TeacherService
    , private router: Router) {
    this.teachers = this.teacherService.teachersActive.valueChanges();
  }

  ngOnInit() {
  }

  goTeacher(key) {
    this.router.navigate(['/teachers/'+key]);
  }

}
