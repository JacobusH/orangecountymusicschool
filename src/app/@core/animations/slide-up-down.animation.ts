import { trigger, state, animate, transition, style } from '@angular/animations';

export const slideUpDownAnimation =
trigger('slideUpDownAnimation', [
  state('true', style({
    transform: 'translateY(0)'
  })),
  state('false', style({
    transform: 'translateY(auto)'
  })),
  transition('0 <=> 1', animate(300))
]);

