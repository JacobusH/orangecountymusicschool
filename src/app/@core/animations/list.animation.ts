import { animate, state, style, transition, trigger, query, stagger, group } from '@angular/animations';

export const listAnimation = trigger('listAnimation', [
  transition(':enter, 0 => 1', [
    group([
      style({opacity: 0}), animate('0.5s', style({ opacity: 1})), //  parent
      query(':enter, .items, .item',
        [style({opacity: 0, transform: 'translateX(100px)' }), stagger('60ms', animate('600ms ease-out', style({ opacity: 1, transform: 'none' })))], // list
        { optional: true }
      )
    ])
  ]),
  transition(':leave, 1 => 0', [
    group([
      style({opacity: 1}), animate('0.5s', style({ opacity: 0})), //  parent
      query(':leave, .items, .item',
        [style({opacity: 1, transform: 'none' }), stagger('-60ms', animate('600ms ease-out', style({ opacity: 0, transform: 'translateX(100px)' })))], // list
        { optional: true }
      )
    ])
  ])
]);


// export const listAnimation = trigger('listAnimation', [
//   transition('* <=> *', [
//     query(':enter',
//       [style({opacity: 0, transform: 'translateX(100px)' }), stagger('60ms', animate('600ms ease-out', style({ opacity: 1, transform: 'none' })))],
//       { optional: true }
//     ),
//     query(':leave',
//       [style({opacity: 1 }), stagger('-60ms', animate('600ms ease-out', style({ opacity: 0 })))],
//       { optional: true}
//     )
//   ])
// ]);
