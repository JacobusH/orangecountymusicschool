import { animate, state, style, transition, trigger } from '@angular/animations';


export const apparateAnimation = trigger('apparateAnimation', [
  transition(':enter', [
    style({
      opacity: 0
    }),
    animate('.7s', style({ opacity: 1}))
  ])
])
  // transition(':leave', [
  //   style({
  //     opacity: 1
  //   }),
  //   animate('.3s', style({ opacity: 0}))
  // ])
