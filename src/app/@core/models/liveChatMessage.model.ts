export interface LiveChatMessage {
  key: string,
  message: string,
  fromAdmin: boolean,
  isActive: boolean,
  createdAt: Date,
  updatedAt: Date
}
