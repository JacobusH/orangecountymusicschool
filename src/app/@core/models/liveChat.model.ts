import { LiveChatMessage } from 'app/@core/models/liveChatMessage.model';

export interface LiveChat {
  key: string,
  name: string,
  email: string,
  isActive: boolean,
  hasUnreadMessages: boolean,
  createdAt: Date,
  updatedAt: Date
}
