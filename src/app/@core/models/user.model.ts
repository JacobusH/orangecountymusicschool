export interface Roles {
  superAdmin?: boolean,
  admin?: boolean,
  student?: boolean,
  teacher?: boolean,
  guest?: boolean
}

export interface User {
  authID: string,
  authMethod: string,
  authDisplayName: string ,
  authPhotoUrl: string,
  key: string,
  name: string,
  email: string;
  phone: string,
  roles: Roles,
  isActive: boolean,
  createdAt: Date,
  updatedAt: Date
}
