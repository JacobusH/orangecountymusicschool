export interface MenuItem {
  'title': string,
  'icon'?: string,
  'link': string,
  'children'?: MenuItem[]
}

export const MENU_ITEMS: MenuItem[] = [
  {
    title: 'Home',
    icon: 'earth',
    link: 'home'
  },
  {
    title: 'Videos',
    icon: 'home',
    link: 'videos'
  },
  {
    title: 'Teachers',
    icon: 'home',
    link: 'teachers'
  },
  {
    title: 'Join Us',
    icon: 'home',
    link: 'join-us'
  },
  {
    title: 'Explore',
    icon: 'video-outline',
    link: 'about',
    children: [
      {
        title: 'About',
        icon: 'file-document-outline',
        link: 'about'
      },
      {
        title: 'Lessons',
        icon: 'file-document-outline',
        link: 'learntoplay'
      },
      {
        title: 'Testimonials',
        icon: 'file-document-outline',
        link: 'testimonials'
      },
      {
        title: 'Bootcamp',
        icon: 'file-document-outline',
        link: 'bootcamp'
      },
      {
        title: 'FAQ',
        icon: 'file-document-outline',
        link: 'faq'
      },
      {
        title: 'Contact',
        icon: 'file-document-outline',
        link: 'contact'
      },
      {
        title: 'Resources',
        icon: 'file-document-outline',
        link: 'resources'
      },
    ]
  }
];
