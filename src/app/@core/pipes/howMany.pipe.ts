import { Pipe, PipeTransform } from '@angular/core';
import { map } from 'rxjs/operators'

@Pipe({
    name: 'howMany',
    pure: false
})
export class HowManyPipe implements PipeTransform {
    transform(items: any[], howMany: number): any {
        if (!items || !howMany) {
            return items;
        }
        // return items.filter(item => item.title.indexOf(filter.title) !== -1);
        // value.map(data => data.filter(x => x.originalHasBeenSeen === false));
        return items.map(
          x => x.slice(0, howMany)
        )
    }
}
