import { Pipe, PipeTransform } from '@angular/core';
import { map,filter } from 'rxjs/operators';
import { pipe } from 'rxjs';

@Pipe({
  name: 'resourceCategoryFilter'
})
export class ResourceCategoryPipe implements PipeTransform {

  transform(items: any[], criteria: string):any {
    if(criteria == 'all') {
      return items;
    }
    else {
      // return items
      //   .map(items => items.filter(x =>
      //     x.category === criteria
      //   ));
      // return items.filter(x => {
      //   x.category === criteria
      // })
      // return items;

      // return pipe(
      //   map(items => {
      //     let tmp = items as any[];
      //     for(let i = 0; i < tmp.length; i++) {
      //       let cat = tmp[i].category as string;
      //       if(cat.toLocaleLowerCase() == criteria.toLocaleLowerCase()) {
      //         return tmp[i];
      //       }
      //     }
      //   })
      // )

      return items.filter(x => {
        if(x.category == criteria) {
          return x
        }
      })

    }
  }

}
