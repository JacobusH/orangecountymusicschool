import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { MatDialogModule, MatDialogRef } from '@angular/material/dialog';
import { YouTubePlayerModule } from '@angular/youtube-player';

import { AccordionComponent } from './components/accordion-group/accordion/accordion.component';
import { AccordionGroupComponent } from './components/accordion-group/accordion-group.component';
import { AdvertComponent } from './components/advert/advert.component';
import { AlertComponent } from './components/alert/alert.component';
import { AnnouncementsComponent } from './components/announcements/announcements.component';
import { DisplayTeacherComponent } from './components/display-teacher/display-teacher.component';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { ImageGalleryComponent } from './components/image-gallery/image-gallery.component';
import { PageHeaderComponent } from './components/page-header/page-header.component';
import { VideoItemComponent } from './components/video-item/video-item.component';

import {
  TimesPipe, CapitalizePipe, NumberWithCommasPipe, PluralPipe, HowManyPipe
  , TimingPipe, RoundPipe, ImageFilterPipe, SafePipe
  , ReadFilterPipe, ResourceCategoryPipe, RoundUpPipe, SignupFilterPipe
} from './pipes/_index';

const COMPONENTS = [
  AccordionComponent
  , AlertComponent
  , AccordionGroupComponent
  , AdvertComponent
  , AnnouncementsComponent
  , DisplayTeacherComponent
  , FooterComponent
  , HeaderComponent
  , ImageGalleryComponent
  , PageHeaderComponent
  , VideoItemComponent
];

const PIPES = [
  TimesPipe
  , CapitalizePipe
  , HowManyPipe
  , NumberWithCommasPipe
  , PluralPipe
  , TimingPipe
  , RoundPipe
  , ImageFilterPipe
  , SafePipe
  , ReadFilterPipe
  , ResourceCategoryPipe
  , RoundUpPipe
  , SignupFilterPipe
];

const MODULES = [
  MatDialogModule
  ,YouTubePlayerModule
];

@NgModule({
  declarations: [
    ... COMPONENTS
    , ... PIPES

  ],
  providers: [
    {
      provide: MatDialogRef,
      useValue: {}
    },
  ],
  imports: [
    ... MODULES
    , CommonModule
    , RouterModule
  ],
  exports: [
    ... COMPONENTS
    , ... PIPES
    , ... MODULES
  ]
})
export class CoreModule { }
