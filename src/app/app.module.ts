import { AppComponent } from './app.component';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { AngularFireModule } from '@angular/fire';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { RouterModule } from '@angular/router';

import { AdminModule } from './@admin/@admin.module';
import { CoreModule } from './@core/@core.module';
import { FrontendModule } from './@frontend/@frontend.module';
import { UserModule } from './@user/@user.module';

import { environment } from '../environments/environment';
import { AppRoutingModule } from './app-routing.module';

const MODULES = [
  AdminModule
  , CoreModule
  , FrontendModule
  , UserModule
];

// Initialize Firebase
var firebaseConfig = {
  apiKey: "AIzaSyCFToauOWTjn55Oc2e6L1YkCt5ZGzbMXV8",
  authDomain: "ocmusicschool-11817.firebaseapp.com",
  databaseURL: "https://ocmusicschool-11817.firebaseio.com",
  projectId: "ocmusicschool-11817",
  storageBucket: "ocmusicschool-11817.appspot.com",
  messagingSenderId: "202663817255"
};

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule
    , AppRoutingModule
    , AngularFireModule
    , AngularFireModule.initializeApp(firebaseConfig)
    , BrowserAnimationsModule
    , RouterModule
    , ... MODULES
  ],
  exports: [
    RouterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
