import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { AdminComponent } from './admin.component';
import { AdminAnnouncementsComponent } from './components/admin-announcements/admin-announcements.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';

import { AdminGuard } from 'app/@core/guard/admin.guard';
import { SuperAdminGuard } from 'app/@core/guard/super-admin.guard';

const routes: Routes = [{
  path: '',
  component: AdminComponent,
  canActivate: [ AdminGuard ],
  children: [
    { path: 'dashboard', component: DashboardComponent},
    { path: 'announcements', component: AdminAnnouncementsComponent },
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full',
    },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [
    RouterModule
  ],
})
export class AdminRoutingModule {
}
