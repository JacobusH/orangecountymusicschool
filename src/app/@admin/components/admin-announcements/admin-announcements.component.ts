import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { Announcement } from 'app/@core/models/_index';
import { AnnouncementService } from 'app/@core/services/_index';
import { NgForm } from '@angular/forms';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-admin-form-announcements',
  templateUrl: './admin-announcements.component.html',
  styleUrls: ['./admin-announcements.component.scss'],
  encapsulation: ViewEncapsulation.Emulated
})
export class AdminAnnouncementsComponent implements OnInit {
  model = this.announcementService.createNew();
  selectedAnnouncement: Announcement;
  announcements: Observable<{}[]>;
  view: any;
  showForm: boolean = false;

  constructor(private announcementService: AnnouncementService) {
    this.announcements = this.announcementService.announcements.valueChanges();
  }

  ngOnInit() {
  }

  saveAnnouncement(form: NgForm) {
    // uploading new teacher with picture
    if(this.selectedAnnouncement == null) {
      this.announcementService.save(this.model)
      this.model = this.announcementService.createNew();
      form.reset();
    }
    // editing testimonial
    else {
      this.announcementService.edit(this.selectedAnnouncement)
      form.reset();
    }
  }

  setNewAnnouncement() {
    this.selectedAnnouncement = null;
    this.model = this.announcementService.createNew();
    this.showForm = true;
  }

  setSelectedAnnouncement(announcement: Announcement) {
    this.selectedAnnouncement = announcement;
    this.model = announcement;
    this.showForm = true;
  }

  deleteAnnouncement(form: NgForm) {
    form.reset();
    this.announcementService.delete(this.selectedAnnouncement);
  }
}
