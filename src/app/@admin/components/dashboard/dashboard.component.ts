import { Component, OnInit, ViewChild, HostListener } from '@angular/core';
import { ContactMessageService, SignupService } from 'app/@core/services/_index';
import { ContactMessage, Signup } from 'app/@core/models/_index';
import { slideUpDownAnimation, apparateAnimation, inOutAnimation, listAnimation, dashAnimation } from 'app/@core/animations/_index';
import { take, map, startWith, takeUntil } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { Observable, combineLatest } from 'rxjs';

@Component({
  selector: 'dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
  animations: [ slideUpDownAnimation, apparateAnimation, inOutAnimation, listAnimation, dashAnimation ]
})
export class DashboardComponent implements OnInit {
  @ViewChild('contactHeader', {static: false}) contactHeader: any;
  @ViewChild('contactBig', {static: false}) contactBig: any;
  isCollapsedSignup = false;
  isCollapsedContact = false;
  windowWidth;
  isShowMoreInfo = false;
  isShowFullMsg = false;


  contactMessages$: Observable<ContactMessage[]>;
  contactMessagesFiltered$: Observable<ContactMessage[]>;
  signups$: Observable<Signup[]>;
  signupsFiltered$: Observable<Signup[]>;
  filter: FormControl;
  filter$: Observable<string>;
  howMany: FormControl;
  howMany$: Observable<number>;

  myMany;

  constructor(
    private contactService: ContactMessageService
    , private signupService: SignupService) {

  }

  ngOnInit() {
    this.windowWidth  = window.innerWidth;
    this.howMany      = new FormControl('');
    this.howMany$     = this.howMany.valueChanges.pipe(startWith(10));
    this.filter       = new FormControl('');
    this.filter$      = this.filter.valueChanges.pipe(startWith(''));

    this.contactMessages$ = this.contactService.contactMessages.valueChanges();
    this.contactMessagesFiltered$ = combineLatest(this.contactMessages$, this.filter$).pipe(
      map(([msgs, filterString]) =>
      msgs.filter(msg =>
           (msg.email && msg.email.toLowerCase().indexOf(filterString.toLowerCase()) !== -1)
        || (msg.phone && msg.phone.toLowerCase().indexOf(filterString.toLowerCase()) !== -1)
        || (msg.name && msg.name.toLowerCase().indexOf(filterString.toLowerCase()) !== -1)
        || (msg.message && msg.message.toLowerCase().indexOf(filterString.toLowerCase()) !== -1 )
      ))
    );
    this.signups$ = this.signupService.signupsRecentFirst.valueChanges();
    this.signupsFiltered$ = combineLatest(this.signups$, this.filter$).pipe(
      map(([signups, filterString]) =>
      signups.filter(signup =>
           (signup.email && signup.email.toLowerCase().indexOf(filterString.toLowerCase()) !== -1)
        || (signup.phone && signup.phone.toLowerCase().indexOf(filterString.toLowerCase()) !== -1)
        || (signup.name && signup.name.toLowerCase().indexOf(filterString.toLowerCase()) !== -1)
        || (signup.instrument && signup.instrument.toLowerCase().indexOf(filterString.toLowerCase()) !== -1)
        || (signup.about && signup.about.toLowerCase().indexOf(filterString.toLowerCase()) !== -1 )
      ))
    );

    // Listen for changes of how many to let in filter
    this.howMany$.subscribe(x => {
      this.myMany = x;
    })

  }

  toggle(which: string) {
    if(which === 'signup') { this.isCollapsedSignup = !this.isCollapsedSignup; }
    if(which === 'contact') { this.isCollapsedContact = !this.isCollapsedContact; }
  }

  @HostListener('window:resize', ['$event']) onResize(event) {
    this.windowWidth = event.target.innerWidth;
    // console.log(event.target.innerWidth);
  }

  showMoreInfo() {
    this.isShowMoreInfo = !this.isShowMoreInfo;
  }

  showFullMsg() {
    this.isShowFullMsg = !this.isShowFullMsg;
  }



}
