import { MenuItem } from 'app/@core/models/menuItem.model';

export const MENU_ITEMS: MenuItem[] = [
  {
    title: 'Front',
    icon: 'earth',
    link: '/',
    home: true,
  },
  {
    title: 'Dashboard',
    icon: 'home',
    link: '/admin/dashboard',
    home: true,
  },
  {
    title: 'Advert',
    icon: 'file-outline',
    link: '/admin/advert',
    home: true,
  },
  {
    title: 'Announcements',
    icon: 'calendar-text-outline',
    link: '/admin/announcements',
    home: true,
  },
  {
    title: 'Chat',
    icon: 'wechat',
    link: '/admin/chat',
    home: true,
  },
  {
    title: 'FAQ',
    icon: 'frequently-asked-questions',
    link: '/admin/references',
    home: true,
  },
  {
    title: 'Gallery',
    icon: 'picture-in-picture-bottom-right',
    link: '/admin/gallery',
    home: true,
  },
  {
    title: 'Joins',
    icon: 'briefcase',
    link: '/admin/join-us',
    home: true,
  },
  {
    title: 'Messages',
    icon: 'forum',
    link: '/admin/messages',
    home: true,
  },
  {
    title: 'Resources',
    icon: 'semantic-web',
    link: '/admin/resources',
    home: true,
  },
  {
    title: 'Sign Up',
    icon: 'hand-right',
    link: '/admin/signup',
    home: true,
  },
  {
    title: 'Teachers',
    icon: 'school',
    link: '/admin/teachers',
    home: true,
  },
  {
    title: 'Testimonials',
    icon: 'message-draw',
    link: '/admin/testimonials',
    home: true,
  },
  {
    title: 'Videos',
    icon: 'videos',
    link: '/admin/videos',
    home: true,
  },
];
