import { AdminRoutingModule } from './admin-routing.module';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CoreModule } from 'app/@core/@core.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatIconModule } from '@angular/material/icon';

import { HeaderAdminComponent } from 'app/@admin/components/header-admin/header-admin.component';
import { FooterAdminComponent } from 'app/@admin/components/footer-admin/footer-admin.component';
import { AdminComponent } from './admin.component';
import { AdminAnnouncementsComponent } from './components/admin-announcements/admin-announcements.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';

export const COMPONENTS = [
  AdminComponent
  , DashboardComponent
  , FooterAdminComponent
  , HeaderAdminComponent
  , AdminAnnouncementsComponent
];

const MODULES = [
  AdminRoutingModule,
  CommonModule,
  CoreModule,
  RouterModule,
  FormsModule,
  ReactiveFormsModule
];


@NgModule({
  declarations: [
    ... COMPONENTS
  ],
  imports: [
    ... MODULES
  ]
})
export class AdminModule {
}
