import { Component, OnInit, HostListener, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router}  from '@angular/router';

import { MENU_ITEMS } from './admin-menu';
import { AuthService } from 'app/@core/services/auth.service';
import { SidebarService } from 'app/@core/services/sidebar.service';
import { Subscription } from 'rxjs';


@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['admin.component.scss']
})
export class AdminComponent implements OnInit, OnDestroy {
  title: string;
  isSidebarLarge: boolean;
  isSidebarMedium: boolean;
  isSidebarNone: boolean;
  isSidebarToggleShow: boolean;
  windowWidth: number;
  router$: Subscription;
  auth$: Subscription;

  menu = MENU_ITEMS;

  constructor(
    private actRoute: ActivatedRoute
    , private auth: AuthService
    , private router: Router
    , private sidebarService: SidebarService
    ) {
    // let adminIdx = window.location.pathname.indexOf('admin');
    // if(adminIdx != -1) {
    //   this.title = window.location.pathname.substring(adminIdx, window.location.pathname.length)
    // }
    this.router$ = this.router.events.subscribe(x => {
      let adminIdx = window.location.pathname.indexOf('admin');
      if(adminIdx != -1) {
        // this.title = window.location.pathname.substring(adminIdx, window.location.pathname.length)
        this.title = window.location.pathname.substring(window.location.pathname.lastIndexOf('/')+1, window.location.pathname.length).toLocaleUpperCase();
      }
    })
  }

  ngOnInit() {
    this.windowWidth = window.innerWidth;
    this.sidebarService.isSidebarToggleShow = true;
    this.getSidebarSize(this.windowWidth);
    if(this.isSidebarNone) {
      this.sidebarService.isSidebarToggleShow = false;
    }
    // add super admin if applicable
    this.auth.user$.subscribe(x => {
      if(x.roles.superAdmin) {
        let hasSuper = false;
        this.menu.forEach(item => {
          if(item.link == '/admin/super') {
            hasSuper = true;
          }
        })
        if(!hasSuper) {
          this.menu.push({
            title: 'Super Admin',
            icon: 'settings-outline',
            link: '/admin/super',
            home: true,
          })
        }
      }
    });
  }

  ngOnDestroy() {
    this.router$.unsubscribe();
    if(this.auth$) this.auth$.unsubscribe();
  }

  @HostListener('window:resize', ['$event']) onResize(event) {
    this.windowWidth = event.target.innerWidth;
    this.getSidebarSize(this.windowWidth);
    // console.log(event.target.innerWidth);
  }

  toggleSidebar() {
    this.sidebarService.isSidebarToggleShow = !this.sidebarService.isSidebarToggleShow;
  }

  getSidebarToggleStatus() {
    if(!this.isSidebarNone && this.sidebarService.isSidebarToggleShow) {
      return true;
    }
    if(this.isSidebarNone && !this.sidebarService.isSidebarToggleShow) {
      return false;
    }
    else if(!this.sidebarService.isSidebarToggleShow) {
      return false;
    }
    else {
      return true;
    }
  }

  getSidebarSize(ww) {
    if(ww > 666) {
      this.isSidebarLarge = true;
      this.isSidebarMedium = false;
      this.isSidebarNone = false;
      // return 'isSidebarLarge';
    }
    else if(ww <= 666 && ww > 420) {
      this.isSidebarLarge = false;
      this.isSidebarMedium = true;
      this.isSidebarNone = false;
      // return 'isSidebarMedium';
    }
    else if(ww <= 420) {
      this.isSidebarLarge = false;
      this.isSidebarMedium = false;
      this.isSidebarNone = true;
      // return 'isSidebarNone';
    }
  }

}

