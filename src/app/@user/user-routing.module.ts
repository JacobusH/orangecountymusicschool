import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { AdminGuard } from 'app/@core/guard/admin.guard';
import { UserGuard } from 'app/@core/guard/user.guard';

import { UserComponent } from './user.component';
import { ProfileComponent } from './profile/profile.component';
import { LoginComponent } from './forms/login/login.component';
import { RegisterComponent } from './forms/register/register.component';
import { FormsComponent } from './forms/forms.component';
import { CalendarComponent } from './profile/calendar/calendar.component';

const routes: Routes = [{
  path: '',
  component: UserComponent,
  children: [
    { path: 'profile', component: ProfileComponent, canActivate: [UserGuard], children: [
      { path: 'calendar', component: CalendarComponent },
    ]},
    { path: 'forms', component: FormsComponent,
      children: [
        { path: 'login', component: LoginComponent },
        { path: 'register', component: RegisterComponent },
      ]},
    { path: '', redirectTo: 'login', pathMatch: 'full' },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserRoutingModule {
}
