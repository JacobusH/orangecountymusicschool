import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from 'app/@core/@core.module';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { FormsComponent } from './forms/forms.component';
import { ProfileComponent } from './profile/profile.component';
import { LoginComponent } from './forms/login/login.component';
import { RegisterComponent } from './forms/register/register.component';
import { UserComponent } from './user.component';
// import { HeaderUserComponent } from './components/header-user/header-user.component';

import { UserRoutingModule } from './user-routing.module';
import { CalendarComponent } from './profile/calendar/calendar.component';

const COMPONENTS = [
  FormsComponent
  , CalendarComponent
  // , HeaderUserComponent
  , LoginComponent
  , ProfileComponent
  , RegisterComponent
  , UserComponent
]


@NgModule({
  declarations: [
    ... COMPONENTS
  ],
  imports: [
    UserRoutingModule
    , CommonModule
    , CoreModule
    , FormsModule
    , RouterModule
  ]
})
export class UserModule { }
