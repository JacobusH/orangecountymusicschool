import { Component, OnInit, ViewEncapsulation, Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ContactMessage } from 'app/@core/models/_index';
import { ContactMessageService } from 'app/@core/services/_index';
import 'rxjs/add/operator/switchMap'
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
  encapsulation: ViewEncapsulation.Emulated
})
export class ContactComponent implements OnInit {
  model = this.contactService.createNew();
  sub: any;
  id: any;

  constructor(private contactService: ContactMessageService
    , private router: Router
    , private route:ActivatedRoute) {

  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id'];
      if(this.id == 'summercamp') {
        this.model = this.contactService.createNew();
        this.model.message = 'I am interested in coming to Summer Boot Camp!';
      }
      else if(this.id == 'bootcamp') {
        this.model = this.contactService.createNew();
        this.model.message = 'I am interested in joining a Boot Camp!';
      }
      else if(this.id == 'online') {
        this.model = this.contactService.createNew();
        this.model.message = 'I am interested in taking online lessons!';
      }
   });
  }

  saveContactMessage(form: NgForm) {
    let mm: ContactMessage = this.model;

    this.contactService.save(mm);
    this.model = this.contactService.createNew();

    form.reset();
    this.router.navigate(['thanks'], {relativeTo: this.route});
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }


}
