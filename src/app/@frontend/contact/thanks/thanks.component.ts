import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-thanks',
  templateUrl: './thanks.component.html',
  styleUrls: ['./thanks.component.scss'],
  encapsulation: ViewEncapsulation.Emulated
})
export class ThanksComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
