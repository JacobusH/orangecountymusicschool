import { Component, OnInit } from '@angular/core';
import { ResourceService } from 'app/@core/services/_index';
import { Resource } from 'app/@core/models/_index';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-resources',
  templateUrl: './resources.component.html',
  styleUrls: ['./resources.component.scss']
})
export class ResourcesComponent implements OnInit {
  resources: any;

  constructor(private resourceService: ResourceService) {
    // this.resources = resourceService.resources.valueChanges();
    resourceService.resources.valueChanges().subscribe(x =>
      this.resources = x)
  }

  ngOnInit() {
  }

}
