import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

import { FrontendComponent } from './frontend.component';
import { AboutComponent } from './about/about.component';
import { BootcampComponent } from './bootcamp/bootcamp.component';
import { ContactComponent } from './contact/contact.component';
import { ThanksComponent } from './contact/thanks/thanks.component';
import { FaqComponent } from './faq/faq.component';
import { GalleryComponent } from './gallery/gallery.component';
import { HomeComponent } from './home/home.component';
import { JoinUsComponent } from './join-us/join-us.component';
import { JoinThanksComponent } from './join-us/thanks/thanks.component';
import { LearntoplayComponent } from './learntoplay/learntoplay.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ResourcesComponent } from './resources/resources.component';
import { TeachersComponent } from './teachers/teachers.component';
import { TeacherDetailComponent } from './teachers/detail/detail.component';
import { TestimonialsComponent } from './testimonials/testimonials.component';
import { VideosComponent } from './videos/videos.component';

export const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'about', component: AboutComponent },
  { path: 'bootcamp', component: BootcampComponent },
  { path: 'faq', component: FaqComponent },
  { path: 'gallery', component: GalleryComponent },
  { path: 'home', component: HomeComponent },
  { path: 'join-us', component: JoinUsComponent },
  { path: 'join-us/thanks', component: JoinThanksComponent },
  { path: 'learntoplay', component: LearntoplayComponent },
  { path: 'learntoplay/:id', component: LearntoplayComponent },
  { path: 'resources', component: ResourcesComponent },
  { path: 'teachers', component: TeachersComponent },
  { path: 'teachers/:id', component: TeacherDetailComponent },
  { path: 'testimonials', component: TestimonialsComponent },
  { path: 'videos', component: VideosComponent },
  { path: 'contact', component: ContactComponent},
  { path: 'contact/thanks', component: ThanksComponent},
  { path: 'contact/:id', component: ContactComponent },
  { path: '**', component: PageNotFoundComponent }
  ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FrontendRoutingModule {
}
