import { AngularFirestore, AngularFirestoreDocument, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Component, OnInit, ViewEncapsulation, Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { FAQ } from 'app/@core/models/_index';
import { FAQService } from 'app/@core/services/_index';
import * as firebase from 'firebase/app';
import 'rxjs/add/operator/switchMap'

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss'],
  encapsulation: ViewEncapsulation.Emulated
})
export class FaqComponent implements OnInit {
  // testimonials: AngularFirestoreCollection<Testimonial>;
  faqs: Observable<FAQ[]>;
  public carouselTileItems: Array<any>;

  constructor(private afs: AngularFirestore, private testServ: FAQService) {
    this.faqs = this.testServ.faqs.valueChanges();
  }

  ngOnInit() {

  }


}
