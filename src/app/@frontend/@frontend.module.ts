import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule }   from '@angular/forms';
import { RouterModule } from '@angular/router';

import { AboutComponent } from './about/about.component';
import { BootcampComponent } from './bootcamp/bootcamp.component';
import { ContactComponent } from './contact/contact.component';
import { ThanksComponent } from './contact/thanks/thanks.component';
import { FaqComponent } from './faq/faq.component';
import { GalleryComponent } from './gallery/gallery.component';
import { HomeComponent } from './home/home.component';
import { JoinUsComponent } from './join-us/join-us.component';
import { JoinThanksComponent } from './join-us/thanks/thanks.component';
import { LearntoplayComponent } from './learntoplay/learntoplay.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ResourcesComponent } from './resources/resources.component';
import { TeachersComponent } from './teachers/teachers.component';
import { TeacherDetailComponent } from './teachers/detail/detail.component';
import { TestimonialsComponent } from './testimonials/testimonials.component';
import { VideosComponent } from './videos/videos.component';

import { HomeGalleryComponent } from './home/home-gallery/home-gallery.component';
import { HomeLearntoplayComponent } from './home/home-learntoplay/home-learntoplay.component';
import { HomeNewsComponent } from './home/home-news/home-news.component';
import { HomeSplashComponent } from './home/home-splash/home-splash.component';
import { HomeTeachersComponent } from './home/home-teachers/home-teachers.component';
import { HomeTestimonialsComponent } from './home/home-testimonials/home-testimonials.component';
import { HomeTilesComponent } from './home/home-tiles/home-tiles.component';
import { HomeVideoComponent } from './home/home-video/home-video.component';


import { CoreModule } from 'app/@core/@core.module';
import { FrontendRoutingModule } from './frontend-routing.module';

const COMPONENTS = [
  AboutComponent, ContactComponent
  , ThanksComponent, FaqComponent
  , GalleryComponent, HomeComponent
  , JoinUsComponent, JoinThanksComponent
  , LearntoplayComponent, BootcampComponent
  , PageNotFoundComponent, ResourcesComponent
  , TeachersComponent, TeacherDetailComponent
  , TestimonialsComponent, VideosComponent
  , HomeGalleryComponent, HomeLearntoplayComponent
  , HomeNewsComponent, HomeSplashComponent
  , HomeTeachersComponent, HomeTestimonialsComponent
  , HomeTilesComponent, HomeVideoComponent
];

@NgModule({
  declarations: [
    ... COMPONENTS
  ],
  imports: [
    CommonModule
    , CoreModule
    , FormsModule
    , RouterModule
    , FrontendRoutingModule
  ],
  exports: [
    ... COMPONENTS
    , RouterModule
  ]
})
export class FrontendModule { }
